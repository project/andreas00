<?php

  /**
  * Implementation of THEMEHOOK_settings() function.
  *
  * @param $saved_settings
  *   array An array of saved settings for this theme.
  * @return
  *   array A form array.
  */
  function phptemplate_settings($saved_settings) {
    /*
    * The default values for the theme variables. Make sure $defaults exactly
    * matches the $defaults in the template.php file.
    */
    $defaults = array(
      'andreas00_primary_links_display_loc' => 'sidebar',
    );

    // Merge the saved variables and their default values
    $settings = array_merge($defaults, $saved_settings);

    // Create the form widgets using Forms API
    $form['andreas00_primary_links_display_loc'] = array(
      '#type' => 'radios',
      '#title' => t('Location of Primary Links'),
      '#options' => array(
        'tabs' => t('Psudo-tabs below the header graphic'),
        'left' => t('List on the left sidebar'),
        'right' => t('List on the right Sidebar'),
      ),
      '#default_value' => $settings['andreas00_primary_links_display_loc'],
    );

    // Return the additional form widgets
    return $form;
  }
?>