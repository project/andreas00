<?php // $Id$ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
  <?php echo $head; ?>
  <?php echo $styles; ?>
  <?php echo $scripts; ?>
  <title><?php echo $head_title; ?></title>
</head>

<body>
<div id="wrap">
  <div id="header">
    <?php
      if ($site_name) {
        echo '<h1><a href="' . $base_path . '">' . check_plain($site_name) . '</a></h1>';
      }

      if ($site_slogan) {
        echo '<p><strong>' . check_plain($site_slogan) . '</strong></p>';
      }
    ?>
  </div>

  <!-- Primary Links -->
  <?php
    if (isset($primary_links)) {
      $plinksloc = theme_get_setting('andreas00_primary_links_display_loc');
      if ($plinksloc == 'tabs') {
        // Pseudo-tabs
        echo theme('links', $primary_links, array('class' => 'links primary-links avmenu'));
      }
      else if ($plinksloc == 'left') {
        // Menu thingie on the left side
        echo '<div id="leftside">' . theme('links', $primary_links, array('class' => 'links primary-links avmenu')) . '</div>';
      }
      else if ($plinksloc == 'right') {
        // Menu thingie on the right side
        echo '<div id="extras">' . theme('links', $primary_links, array('class' => 'links primary-links avmenu')) . '</div>';
      }
    }
  ?>

  <!-- Left Sidebar -->
  <?php
    if ($left) {
      echo '<div id="leftside">';
        echo $left;
      echo '</div>';
    }
  ?>

  <!-- Right Sidebar -->
  <?php
    if ($tabs) {
      echo '<div id="extras">';
        echo theme('menu_local_tasks');
      echo '</div>';
    }
  ?>

  <?php
    if ($right) {
      echo '<div id="extras">';
        echo $right;
      echo '</div>';
    }
  ?>

  <?php
    if (!$right && $plinksloc != 'right' && !$tabs) {
      echo '<div id="contentwide">';
    }
    else if ($right || $plinksloc == 'right' || $tabs) {
      echo '<div id="content">';
    }
   ?>
    <h2><a href="#"><?php echo $title; ?></a></h2>
    <?php echo $content; ?>
  </div>

  <div id="footer">
    <?php echo $footer; ?>
    <?php echo $footer_message; ?>
    <p>Design by <a href="http://andreasviklund.com/">Andreas Viklund</a> | Drupal Port by <a href="http://dankeenan.org">dk</a>| <a href="http://www.xnavigation.net/" title="xNavigation gratis download - La risorsa italiana sul software">xNavigation</a></p>
  </div>

</div>
<?php echo $closure; ?>
</body>
</html>
