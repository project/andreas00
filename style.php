<?php

  // Drupalisms are not included in this CSS file due to the way it is called, so we need to get the image some other way
  $headimg = $_GET['headimg'];

  // Return CSS, not (X)HTML
  header('Content-type: text/css');

  echo <<<_CSS
#content {
line-height:1.5em;
margin:10px 160px;
padding:0;
text-align:left;
}

#content h2,#contentwide h2 {
font-size:1.8em;
font-weight:400;
letter-spacing:-1px;
margin:8px 0 10px;
padding:0;
}

#content h3,#contentwide h3 {
font-size:1.5em;
font-weight:400;
margin:6px 0;
padding:0;
}

#content img,#contentwide img {
background:#ccc;
border:4px solid #f0f0f0;
color:#303030;
display:inline;
margin:0 10px 5px 0;
padding:1px;
}

#content li,#contentwide li {
margin:0 0 2px 10px;
padding:0 0 0 4px;
}

#content ul ul,#content ol ol,#contentwide ul ul,#contentwide ol ol {
margin:2px 0 2px 20px;
}

#content ul,#content ol,#contentwide ul,#contentwide ol {
margin:0 0 16px 20px;
padding:0;
}

#contentwide {
line-height:1.5em;
margin:10px 0 10px 160px;
padding:0;
text-align:left;
}

#extras {
clear:right;
float:right;
margin:0 0 10px;
padding:0;
width:150px;
}

#extras h2 {
font-size:1.6em;
font-weight:400;
letter-spacing:-1px;
margin:0 0 6px;
}

#extras input,#leftside input {
max-width:150px;
}

#extras li {
list-style:none;
margin:0 0 6px;
padding:0;
}

#extras p,#extras ul {
font-size:0.9em;
line-height:1.3em;
margin:0 0 1.5em;
padding:0;
}

#extras ul.avmenu li a {
background:url(img/menubg.gif) repeat-x bottom right #f4f4f4;
border-bottom:1px solid #d8d8d8;
border-left:1px solid #d8d8d8;
border-right:4px solid #ccc;
border-top:1px solid #d8d8d8;
color:#505050;
float:right;
font-size:1em;
font-weight:700;
margin-bottom:5px;
padding:1px 5px 5px;
text-decoration:none;
width:138px;
}

#extras ul.avmenu li a:hover,#extras ul.avmenu li a.current {
background:url(img/menubg2.gif) repeat-x bottom right #eaeaea;
border-bottom:1px solid #b0b0b0;
border-left:1px solid #b0b0b0;
border-right:4px solid #505050;
border-top:1px solid #b0b0b0;
color:#505050;
}

#footer {
background:#fff;
border-top:2px solid #dadada;
clear:both;
color:gray;
font-size:0.9em;
margin:0 auto;
padding:8px 0;
text-align:center;
width:760px;
}

#footer a {
background:inherit;
color:gray;
font-weight:400;
text-decoration:none;
}

#footer a:hover {
text-decoration:underline;
}

#footer p {
margin:0;
padding:0;
}

#header {
background:url($headimg) no-repeat bottom left #fff;
color:#505050;
height:250px;
margin:0 0 10px;
padding:0;
}

#header h1 {
font-size:2.5em;
font-weight:400;
letter-spacing:-2px;
margin:0 0 4px 15px;
padding:15px 0 0;
}

#header h1 a,#header h1 a:hover {
font-weight:400;
padding:0;
}

#header p {
font-size:1.1em;
letter-spacing:-1px;
margin:0 0 20px 15px;
padding:0 0 0 3px;
}

#leftside {
clear:left;
float:left;
margin:0 0 10px;
padding:0;
width:150px;
}

#leftside .announce {
background:url(img/menubg.gif) repeat-x bottom left #f4f4f4;
border-bottom:1px solid #d8d8d8;
border-left:4px solid #ccc;
border-right:1px solid #d8d8d8;
border-top:1px solid #d8d8d8;
clear:left;
color:#505050;
line-height:1.3em;
margin:10px 0;
padding:5px;
width:134px;
}

#leftside h2 {
font-size:1.5em;
font-weight:400;
}

#leftside p,.announce p {
font-size:0.9em;
}

#leftside ul.avmenu li a {
background:url(img/menubg.gif) repeat-x bottom left #f4f4f4;
border-bottom:1px solid #d8d8d8;
border-left:4px solid #ccc;
border-right:1px solid #d8d8d8;
border-top:1px solid #d8d8d8;
color:#505050;
float:left;
font-size:1em;
font-weight:700;
margin-bottom:5px;
padding:5px 1px 5px 5px;
text-decoration:none;
width:138px;
}

#leftside ul.avmenu li a:hover,#leftside ul.avmenu li a.current {
background:url(img/menubg2.gif) repeat-x bottom left #eaeaea;
border-bottom:1px solid #b0b0b0;
border-left:4px solid #505050;
border-right:1px solid #b0b0b0;
border-top:1px solid #b0b0b0;
color:#505050;
}

#leftside ul.avmenu li,#extras ul.avmenu li {
display:inline;
padding:0;
width:150px;
}

#leftside ul.avmenu ul a,#extras ul.avmenu ul a {
padding:3px 1px 3px 5px;
width:123px;
}

#leftside ul.avmenu ul ul a,#extras ul.avmenu ul ul a {
width:108px;
}

#leftside ul.avmenu ul ul,#extras ul.avmenu ul ul {
font-size:1em;
width:120px;
}

#leftside ul.avmenu ul,#extras ul.avmenu ul {
font-size:0.9em;
margin:0 0 0 15px;
padding:0 0 5px;
width:133px;
}

#leftside ul.avmenu,#extras ul.avmenu {
list-style:none;
margin:0 0 18px;
padding:0;
width:150px;
}

#searchbox {
margin:6px 0 16px;
padding:0;
}

#searchbox label,.hide {
display:none;
}

#searchform {
background:#fff;
border:1px solid #ccc;
color:#505050;
font-size:0.9em;
padding:4px;
width:116px;
}

#wrap {
background:#fff;
color:#303030;
margin:0 auto;
padding:1px 5px;
width:760px;
}

.announce h2 {
margin:0 0 10px;
padding:0;
}

.button {
background:url(img/menubg.gif) repeat-x bottom left #f4f4f4;
border-bottom:1px solid #d8d8d8;
border-left:4px solid #ccc;
border-right:1px solid #d8d8d8;
border-top:1px solid #d8d8d8;
color:#505050;
font-weight:700;
margin:0 0 15px;
padding:7px 7px 7px 11px;
width:120px;
}

.center {
text-align:center;
}

.highlighted {
background:#f0f0f0;
border:1px solid #b0b0b0;
color:#303030;
padding:3px;
}

.large {
font-size:1.3em;
}

.left {
float:left;
margin:10px 10px 5px 0;
}

.new .marker {
color:#ff0000;
float:right;
font-size:0.92em;
font-weight:bold;
}

.right {
float:right;
margin:10px 0 5px 10px;
}

.small {
font-size:0.8em;
}

.textright {
margin:-10px 0 4px;
text-align:right;
}

a {
background:inherit;
color:#166090;
font-weight:700;
text-decoration:none;
}

a img {
border:0;
}

a:hover {
background:inherit;
color:#286ea0;
text-decoration:underline;
}

blockquote {
background:url(img/menubg.gif) repeat-x bottom left #f4f4f4;
border-bottom:1px solid #d8d8d8;
border-left:4px solid #ccc;
border-right:1px solid #d8d8d8;
border-top:1px solid #d8d8d8;
color:#505050;
margin:16px;
padding:7px 7px 7px 11px;
}

blockquote p {
font-size:1.1em;
line-height:1.3em;
margin:0;
}

body {
background:url(img/bg.gif) repeat-y top center #eaeaea;
color:#3a3a3a;
font:76% Verdana,Tahoma,Arial,sans-serif;
margin:0 auto;
padding:0;
}

caption {
font-size:1.5em;
font-weight:400;
margin:0;
padding:6px 0 8px;
text-align:left;
}

input {
width:200px;
}

input,textarea {
border:1px solid #ccc;
font-family:Verdana,Tahoma,Arial,Sans-Serif;
font-size:1em;
margin:0;
padding:4px;
}

label {
margin:2px;
}

p {
margin:0 0 16px;
}

table {
border:1px solid #d8d8d8;
border-collapse:collapse;
line-height:1.3em;
margin:0 0 16px;
padding:0;
width:95%;
}

td {
background:url(img/menubg.gif) repeat-x bottom left #f4f4f4;
color:#303030;
font-size:0.9em;
padding:7px;
text-align:left;
}

textarea {
width:400px;
}

th {
background:url(img/menubg2.gif) repeat-x bottom left #eaeaea;
color:#505050;
padding:7px;
text-align:left;
}
_CSS;
?>