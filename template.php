<?php
  // $ Id: $

  if (theme_get_setting('default_logo')) {
    $logo = 'img/front.jpg';
  }
  else {
    $logo = $base_path . '/' . theme_get_setting('logo_path');
  }

  drupal_add_css(path_to_theme() . '/style.php?headimg=' . $logo, 'theme');

  /**
   * Initialize theme settings
   */
  if (is_null(theme_get_setting('andreas00_primary_links_display_loc'))) {
    global $theme_key;

    /**
     * The default values for the theme variables. Make sure $defaults exactly
     * matches the $defaults in the theme-settings.php file.
     */
    $defaults = array(
      'andreas00_primary_links_display_loc' => 'sidebar',
    );

    // Save theme settings with the defaults
    variable_set(
      str_replace('/', '_', 'theme_'. $theme_key .'_settings'),
      array_merge($defaults, theme_get_settings($theme_key))
    );
    // Force refresh of Drupal internals
    theme_get_setting('', TRUE);
  }
  /**
  * Return a themed marker, useful for marking new or updated
  * content.
  *
  * @param $type
  *   Number representing the marker type to display
  * @see MARK_NEW, MARK_UPDATED, MARK_READ
  * @return
  *   A string containing the marker.
  */
  function andreas00_mark($type = MARK_NEW) {
    global $user;
    if ($user->uid) {
      if ($type == MARK_NEW) {
        return ' <span class="marker">*'. t('new') .'*</span>';
      }
      else if ($type == MARK_UPDATED) {
        return ' <span class="marker">*'. t('updated') .'*</span>';
      }
    }
  }

  function andreas00_menu_local_tasks() {
    $output = '';

    if ($primary = menu_primary_local_tasks()) {
      $output .= '<h2>' . t('Actions') . '</h2>';
      $output .= "<ul class=\"links primary-links avmenu\">\n". $primary ."</ul>\n";
    }
    // Don't handle secondary links
    //   if ($secondary = menu_secondary_local_tasks()) {
    //     $output .= "<ul class=\"tabs secondary\">\n". $secondary ."</ul>\n";
    //   }

    return $output;
  }